#include <iostream>

using namespace std; // @suppress("Symbol is not resolved")

void v()
{cout << "\\          //" << endl;
 cout << " \\        //" << endl;
 cout << "  \\      //" << endl;
 cout << "   \\    // " << endl;
 cout << "    \\  // " << endl;
 cout << "     \\//" << endl;

}

void i()
{cout << "   0" << endl;
cout << "   ||" << endl;
cout << "   ||" << endl;
cout << "   ||" << endl;
cout << "   ||" << endl;
cout << "   ||" << endl;
cout << "   --" << endl;
}

void k()
{cout << "||   //" << endl;
 cout << "||  //" << endl;
 cout << "|| //" << endl;
 cout << "||//   " << endl;
 cout << "||\\ " << endl;
 cout << "||  \\" << endl;
 cout << "||   \\" << endl;

}

void a()
{
cout << "     //\\ " << endl;
cout << "    //  \\" << endl;
cout << "   //- - \\   " << endl;
cout << "  //      \\" << endl;
cout << " //        \\" << endl;

}


int main()
{
    v();
    i();
    k();
    a();
    return 0;
}
